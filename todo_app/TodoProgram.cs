﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DipsTodoApp
{
    // Honestly this
    public class TodoProgram
    {
        // List of all saved Todo Items
        private List<string> todoItems;
        
        //- SAVESTATE METHODS ---------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Adds a new item to the programs savestate
        /// </summary>
        /// <param name="item"> The item to be added </param>
        private void AddToSaveState(string item)
        {
            DipsJobbProsjekt.Properties.Settings.Default.TodoList.Add(item);
            DipsJobbProsjekt.Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Changes the saved item at a given index
        /// </summary>
        /// <param name="index">the position of the item</param>
        /// <param name="newState">The new state that the item is set to</param>
        private void UpdateSaveStateItem(int index, string newState)
        {
            DipsJobbProsjekt.Properties.Settings.Default.TodoList[index] = newState;
            DipsJobbProsjekt.Properties.Settings.Default.Save();
        }

        /// <summary>
        /// Fetches the items saved in the program settings, and adds them to the internal list
        /// </summary>
        public void LoadState()
        {
            var collection = DipsJobbProsjekt.Properties.Settings.Default.TodoList;
            todoItems = collection.Cast<string>().ToList();
        }

        /// <summary>
        /// Clears the currently saved list. Not supported by a command, but used for the unit tests
        /// </summary>
        public void ResetState()
        {
            DipsJobbProsjekt.Properties.Settings.Default.Reset();
            DipsJobbProsjekt.Properties.Settings.Default.Save();
            LoadState();

        }
        //-----------------------------------------------------------------------------------------------------------------------------------------------------------

        //- BASE FUNCTIONALITY --------------------------------------------------------------------------------------------------------------------------------------
        /// <summary>
        /// Adds a item to the list
        /// </summary>
        /// <param name="newItem">The item to be added</param>
        private string AddItem(string newItem)
        {
            todoItems.Add(newItem);
            AddToSaveState(newItem);

            // Write status message
            return($"Added {PrintItem(todoItems.Count - 1)}");
            
        }

        /// <summary>
        /// Prints the item at a given index
        /// </summary>
        /// <param name="itemIndex">index of the item to be printed</param>
        private string PrintItem(int itemIndex)
        {
            return($"#{itemIndex + 1} {todoItems[itemIndex]}");
        }

        /// <summary>
        /// Goes through the entire list of todo items and prints them
        /// </summary>
        private string PrintAllItems()
        {
            // Iterates through each item, and write them in the console
            var retString = "";
            for (var i = 0; i < todoItems.Count; i++)
            {
                retString += "\n" + PrintItem(i);
            }

            return retString;
        }

        /// <summary>
        /// Marks the item at a given index as 'completed'
        /// </summary>
        /// <param name="itemIndex">index of the item to be marked</param>
        private string CompleteItem(int itemIndex)
        {
            // Ensure the item exists within the list
            if ((0 > itemIndex)||(itemIndex >= todoItems.Count))
            {
                return($"Err: item #{itemIndex + 1} does not exist");
            }
            
            // Ensure that the current item isnt already marked as "done"
            if (!todoItems[itemIndex].StartsWith("DONE "))
            {
                // Write status message
                var retString = $"Completed {PrintItem(itemIndex)}";

                todoItems[itemIndex] = $"DONE {todoItems[itemIndex]}";
                UpdateSaveStateItem(itemIndex, todoItems[itemIndex]);

                return retString;
            } else
            {
                return("Err: item is already completed");
            }
        }

        /// <summary>
        /// Runs various methods, decided by a command input written as [verb] [data]
        /// </summary>
        /// <param name="commandString">A command to be parsed into tokens and handled</param>
        public string HandleCommand(string commandString)
        {
            // Split the string into <verb> <data>
            string[] commandTokens = commandString.Split(new[] { ' ' }, 2, StringSplitOptions.RemoveEmptyEntries);

            // Handle the <verb> token
            switch (commandTokens[0].ToUpper())
            {
                case "ADD":
                    // Catch empty data
                    if(commandTokens.Count() > 1)
                    {
                        return(AddItem(commandTokens[1]));
                    }
                    else
                    {
                        return($"Err: No item to add");
                    }

                case "DO":
                    // The if statement ensures that the <data> token is an integer
                    int index = -1;
                    if (Int32.TryParse(commandTokens[1], out index))
                    {
                        // Users list is 1-indexed, so we subtract 1
                        return (CompleteItem(index - 1));
                    }
                    else
                    {
                        return($"Err: Cannot convert <{commandTokens[1]}> into a number");
                    }

                case "PRINT":
                    return(PrintAllItems());
                default:
                    // Informs the user that their command wasn't recognized
                    return($"Err: Unknown command <{commandTokens[0]}>");
            }
        }

        public string Start()
        {
            LoadState();
            
            // Inform the user if there already is some data
            if (todoItems.Count > 0)
            {
                return PrintAllItems();
            }
            else
            {
                return "";
            }
        }


        //-----------------------------------------------------------------------------------------------------------------------------------------------------------

        //- MAIN METHOD ---------------------------------------------------------------------------------------------------------------------------------------------
        static void Main(string[] args)
        {
            Console.WriteLine(">------------============< TODO LIST >============------------<");
            var todoApp = new DipsTodoApp.TodoProgram();
            var programInfo = todoApp.Start();
            var input = "";

            Console.WriteLine(programInfo);
            while (true){
                Console.Write("\n>  ");
                input = Console.ReadLine();
                programInfo = todoApp.HandleCommand(input);
                Console.WriteLine(programInfo);
            }

        }
    }
}
