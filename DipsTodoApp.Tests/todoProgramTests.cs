﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace DipsTodoApp.Tests
{
    [TestClass]
    public class TodoProgramTests
    {
        [TestMethod]
        public void Test_Legal_Command_ADD()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            // Test that it works
            var command1 = "add Kjøp melk og brød";
            var EXPECTED_OUTPUT1 = "Added #1 Kjøp melk og brød";

            // Test that it increases
            var command2 = "add Lær å lage c-sharp unit-tester";
            var EXPECTED_OUTPUT2 = "Added #2 Lær å lage c-sharp unit-tester";

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);
            
        }

        [TestMethod]
        public void Test_Legal_Command_DO()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            // add some dummy tasks
            for (var i = 0; i < 20; i ++)
            {
                todoProgram.HandleCommand($"add dummy task number {i+1}");
            }

            var command1 = "do 4";
            var EXPECTED_OUTPUT1 = "Completed #4 dummy task number 4";

            var command2 = "do 16";
            var EXPECTED_OUTPUT2 = "Completed #16 dummy task number 16";

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);
        }

        [TestMethod]
        public void Test_Legal_Command_Print()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            // add some dummy tasks
            for (var i = 0; i < 5; i++)
            {
                todoProgram.HandleCommand($"add dummy task");
            }

            var command1 = "print";
            var EXPECTED_OUTPUT1 =
                "\n#1 dummy task\n" +
                "#2 dummy task\n" +
                "#3 dummy task\n" +
                "#4 dummy task\n" +
                "#5 dummy task";

            // Print should ignore whatever is typed behind it (same output as above)
            var command2 = "print Do the dishes";
            var EXPECTED_OUTPUT2 = EXPECTED_OUTPUT1;

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);
        }


        [TestMethod]
        public void Test_Illegal_Arguments_Add()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            var command1 = "Add";
            var EXPECTED_OUTPUT1 = "Err: No item to add";

            var command2 = "Add ";
            var EXPECTED_OUTPUT2 = "Err: No item to add";

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);

        }

        [TestMethod]
        public void Test_Illegal_Arguments_Do()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            // add some dummy tasks
            for (var i = 0; i < 20; i++)
            {
                todoProgram.HandleCommand("add dummy task");
            }

            var command1 = "Do ABCS";
            var EXPECTED_OUTPUT1 = "Err: Cannot convert <ABCS> into a number";

            var command2 = "do -1";
            var EXPECTED_OUTPUT2 = "Err: item #-1 does not exist";

            var command3 = "do 100";
            var EXPECTED_OUTPUT3 = "Err: item #100 does not exist";

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);
            var result3 = todoProgram.HandleCommand(command3);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);
            Assert.AreEqual<string>(EXPECTED_OUTPUT3, result3);

        }

        [TestMethod]
        public void Test_Illegal_UnrecognizedCommands()
        {
            // Arrange
            var todoProgram = new TodoProgram();
            todoProgram.ResetState();

            // add some dummy tasks
            for (var i = 0; i < 5; i++)
            {
                todoProgram.HandleCommand($"add dummy task");
            }

            var command1 = "AddRemember to yada yada";
            var EXPECTED_OUTPUT1 = "Err: Unknown command <AddRemember>";

            // Print should ignore whatever is typed behind it (same output as above)
            var command2 = "anviujfdsnb";
            var EXPECTED_OUTPUT2 = "Err: Unknown command <anviujfdsnb>";

            // Act
            var result1 = todoProgram.HandleCommand(command1);
            var result2 = todoProgram.HandleCommand(command2);

            // Assert
            Assert.AreEqual<string>(EXPECTED_OUTPUT1, result1);
            Assert.AreEqual<string>(EXPECTED_OUTPUT2, result2);
        }
    }
}
